# SSSD, Multiple Domains

This is a brief guide on how to integrate a Linux host to two or more Active Directory domains using SSSD. It's very common for companies to field MSADs as the domain manager in their environments and as such, it's usual for Linux System Administrators to have to either deploy an IdM in the same environment, or if the server count is reasonably low, integrate the servers using services such as SSSD or Winbind.

Since I've been playing with it for a while now, I've decided to write this brief guide to help new sysadmins and lessen their suffering when dealing with authentication, as we've all been there once.

To Do:
- Add the sample config files
- Translation
