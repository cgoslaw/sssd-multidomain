# RHEL8 SSSD Setup - Multi Domains
This is but a brief guide on integrating a Linux Server with MS Active Directory. I'll be using a RHEL server but it may be similar for other distributions as well.

For this process to work, we're going to use the System Security Services Daemon (SSSD), which acts as an intermediary between local clients and remote identity and auth providers, being able to cache credentials in case the host or the remote identity provider goes offline.

And although it's good to have some knowledge on LDAP, Kerberos and MSAD to understand all the process, it's not imperative as it's not that hard if you prepare before deploying it to production.

As a note, this guide can be replicable in a lab with a Linux Server (EL8), two MSADs 2019 and a DNS server.

## Things to consider:
- SSSD supports only domains in a single AD forest. If access to multiple domains from multiple forests are needed, it's better to consider IdM with trusts or winbindd instead of SSSD.
- Ports required for direct integration of Linux Systems to AD using SSSD:
    - DNS, 53 (UDP, TCP)
    - LDAP, 389 (UDP, TCP)
    - Kerberos, 88 (UDP, TCP)
    - Kerberos, 464 (UDP, TCP) - Used by kadmin for setting and changing passwd
    - LDAP Global Catalog, 3268 (TCP) - If the **id_provider = ad** is being used
    - NTP, 123 (UDP) - Optional
## The Process
1. First we install the necessary packages:
```
# dnf install realmd oddjob oddjob-mkhomedir sssd adcli krb5-workstation
```
2. Use realm to verify if the first domain is discoverable via DNS:
```
# realm -v discover ad.example.net

 * Resolving: _ldap._tcp.ad.example.net
 * Resolving: ad.example.net
 * Performing LDAP DSE lookup on: 10.1.1.101
 * Performing LDAP DSE lookup on: 10.1.1.102
 * Successfully discovered: ad.example.net
ad.example.net
  type: kerberos
  realm-name: AD.EXAMPLE.NET
  domain-name: ad.example.net
  configured: no
  server-software: active-directory
  client-software: sssd
  required-package: oddjob
  required-package: oddjob-mkhomedir
  required-package: sssd
  required-package: adcli
  required-package: samba-common-tools
```
3. Since the first domain can be found via DNS, we can simply join the first domain with ***realm join***. Realm automatically creates a sssd configuration, adding pam and nss and starting the necessary services.

*Note: By default, realm uses the domain's admin to perform the computer creation operation. In case another privileged account is needed, pass the -U/--user option:*
```
# realm join -v ad.example.net --computer-ou="Non-Critical Servers,ou=Linux Servers,ou=Production Servers,ou=Servers,ou=Example,dc=ad,dc=example,dc=net" -U "admin@AD.EXAMPLE.NET"
<output omitted>
```
4. Now, since realm cannot be used to join and automate the configuration to a secondary domain, we're going to have to do everything manually. We can start by configuring the /etc/krb5.conf and include the secondary domain in the configuration file so we can later request a Kerberos TGT:
```
[logging]
    default = FILE:/var/log/krb5libs.log
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmind.log

[libdefaults]
    dns_lookup_realm = false
    ticket_lifetime = 24h
    renew_lifetime = 7d
    forwardable = true
    rdns = false
    pkinit_anchors = FILE:/etc/pki/tls/certs/ca-bundle.crt
    spake_preauth_groups = edwards25519
    default_ccache_name = KEYRING:persistent:%{uid}
    udp_preference_limit = 0

[realms]
AD.EXAMPLE.NET = {
     kdc = msdc01.ad.example.net:88
     admin_server = ad.example.net
}
DIR.SAMPLE.NET = {
    kdc = msdc01.dir.sample.net:88
    admin_server = msdc01.dir.sample.net

[domain_realm]
.ad.example.net = AD.EXAMPLE.NET
ad.example.net = AD.EXAMPLE.NET
.dir.sample.net = DIR.SAMPLE.NET
dir.sample.net = DIR.SAMPLE.NET
```
5. Assuming your DNS can only resolve the **AD.EXAMPLE.NET** addresses, and you haven't been granted much info about the new domain aside from the DNS address (10.2.2.120 in our guide), you can start by querying the _msdcs zones for the SRV records to locate the AD domain controllers:
```
# dig -t SRV _ldap._tcp.dc._msdcs.dir.sample.net  @10.2.2.120

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> -t SRV _ldap._tcp.dc._msdcs.dir.sample.net @10.2.2.120
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 9134
;; flags: qr rd ra; QUERY: 1, ANSWER: 40, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;_ldap._tcp.dc._msdcs.dir.sample.net. IN SRV

;; ANSWER SECTION:
_ldap._tcp.dc._msdcs.dir.sample.net. 238 IN SRV 0 100 389 msdc01.dir.sample.net.
_ldap._tcp.dc._msdcs.dir.sample.net. 238 IN SRV 0 100 389 msdc02.dir.sample.net.
<output omitted>
```
And then after finding one of the ADDCs, we can query its IP address so we can use it as our main DC:
```
# dig msdc01.dir.sample.net @10.2.2.120
                                                                 
; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> msdc01.dir.sample.net @10.2.2.120
;; global options: +cmd                            
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 53123
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1                             
                                                                                                 
;; OPT PSEUDOSECTION:                                                                            
; EDNS: version: 0, flags:; udp: 4000                                                                                         
;; QUESTION SECTION:                                                                             
;msdc01.dir.sample.net. IN    A                                                        
                                                                                                 
;; ANSWER SECTION:                                                                               
msdc01.dir.sample.net. 753 IN A       10.2.2.101
```
6. We include the specific MSAD Server in the /etc/hosts:
```
# echo "10.2.2.101  msdc01.dir.sample.net   dir.sample.net" >> /etc/hosts
```
7. We can now request the TGT we're going to use in the process to join the server to the secondary domain:
```
# kinit -V admin@DIR.SAMPLE.NET

Using default cache: 0
Using principal: admin@DIR.SAMPLE.NET
Password for admin@DIR.SAMPLE.NET:
Authenticated to Kerberos v5
```
And finally we can join the computer to the domain with adcli:

*Note: We are using* ***--login-ccache*** *for adcli to use that TGT we've requested earlier for authentication with kinit and* ***--host-keytab="<value>"*** *so we can have a separate keytab file.*

*Note: the* ***--domain-controller="<value>"*** *option is important in this case as being unable to query the domain via DNS, passing it directly to adcli makes it look for the address we've previously configured in /etc/hosts*
```
# adcli join --login-ccache --domain-controller=msdc01.dir.sample.net --host-keytab=/etc/krb5.keytab_sample.net --domain-ou="ou=Servers,dc=dir,dc=sample,dc=net" dir.sample.net --show-details

[domain]              
domain-name = dir.sample.net
domain-realm = DIR.SAMPLE.NET                    
domain-controller = msdc01.dir.sample.net
domain-short = DIR              
domain-SID = <omitted>
naming-context = DC=dir,DC=sample,DC=net
domain-ou = ou=Servers,dc=dir,dc=sample,dc=net
[computer]
host-fqdn = server01
computer-name = server01
computer-dn = CN=server01,ou=Servers,dc=dir,dc=sample,dc=net
os-name = redhat-linux-gnu                          
[keytab]
kvno = 2                 
keytab = /etc/krb5.keytab_sample.net
```
8. Now we'll configure the SSSD config file:

*Reminder that since we've used realm to join our server to the first domain, we already have a sample configuration for sssd. Having used adcli for the secondary, we're going to have to configure it from scratch and append it to* ***domains = "<value>"*** *as well.*
```
# vim /etc/sssd/sssd.conf

[sssd]
domains = ad.example.net,dir.sample.net
config_file_version = 2
services = nss, pam

[domain/ad.example.net]
ad_domain = ad.example.net
krb5_realm = AD.EXAMPLE.NET
id_provider = ad
access_provider = simple
realmd_tags = manages-system joined-with-adcli
cache_credentials = True
krb5_store_password_if_offline = True
default_shell = /bin/bash
ldap_id_mapping = True
use_fully_qualified_names = False
fallback_homedir = /home/%d/%u
simple_allow_groups = G_Admin_Server_Server01@AD.EXAMPLE.NET

[domain/dir.sample.net]
ad_server = msdc01.dir.sample.net
ad_domain = dir.sample.net
krb5_realm = DIR.SAMPLE.NET
realmd_tags = manages-system joined-with-adcli
cache_credentials = True
id_provider = ad
krb5_store_password_if_offline = True
default_shell = /bin/bash
ldap_id_mapping = True
use_fully_qualified_names = False
fallback_homedir = /home/%d/%u
access_provider = simple
dyndns_update = false
simple_allow_users = admin
simple_allow_groups =
krb5_keytab = /etc/krb5.keytab_sample.net
ldap_krb5_keytab = /etc/krb5.keytab_sample.net
```
And we can now restart SSSD and check the realm list:
```
# systemctl restart sssd
# realm list
dir.sample.net
  type: kerberos
  realm-name: DIR.SAMPLE.NET
  domain-name: dir.sample.net
  configured: kerberos-member
  server-software: active-directory
  client-software: sssd
  required-package: oddjob
  required-package: oddjob-mkhomedir
  required-package: sssd
  required-package: adcli
  required-package: samba-common-tools
  login-formats: %U
  login-policy: allow-permitted-logins
  permitted-logins: admin
  permitted-groups: 
ad.example.net
  type: kerberos
  realm-name: AD.EXAMPLE.NET
  domain-name: ad.example.net
  configured: kerberos-member
  server-software: active-directory
  client-software: sssd
  required-package: oddjob
  required-package: oddjob-mkhomedir
  required-package: sssd
  required-package: adcli
  required-package: samba-common-tools
  login-formats: %U
  login-policy: allow-permitted-logins
  permitted-logins: 
  permitted-groups: G_Admin_Server_Server01@AD.EXAMPLE.NET
```
Lastly, something important to note is that in case the naming convention for users is identical on both servers, it's better to use the value for **use_fully_qualified_names** as True, so there's no conflict between users from both domains upon login.

And we're done. It's possible now to query the domains for info on users or groups with either getent or ldapsearch to test it or log into the server. In overall that's pretty much it.

For further reference, I also recommend reading through every man page related to the files and commands used here as to understand why and how they are used, and how the conf files can be tweaked to your necessity.

## Final considerations
I really appreciate it if you've got this far.

If you have any comments, corrections or suggestions you consider appropriate, please feel free to contact me.

cgoslaw
